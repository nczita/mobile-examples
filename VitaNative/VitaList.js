import React from 'react';

class VitaList extends React.Component {
    state = {
        saints: [
            {
                "name": "św. Łukasz",
                "desc": "Ewangelista"
            },
            {
                "name": "św. Marek",
                "desc": "Ewangelista"
            },
            {
                "name": "św. Maksymylian",
                "desc": "Męczennik"
            },
            {
                "name": "św. Jan Paweł II",
                "desc": "Papież"
            }
        ]
    }

    render() {
        return (
            <View style={styles.container}>
                {this.state.saints.map(saint =>
                    <Text>{saint.name}</Text>
                )}
            </View>
        );
    }
}

export default VitaList;