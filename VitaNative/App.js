import React from 'react';
import { StyleSheet, Text, View, Image, TextInput, Button, Alert } from 'react-native';

class AlertConfimation extends React.Component {
  constructor(props) {
    super(props);
    this.state = { text: '' };
  }

  getAlertText() {
    let napis = 'oh, nic ... 😔';
    if (this.state.text) {
      napis = 'Mam "' + this.state.text + '" 😀 !!!';
    }
    return napis;
  }

  render() {
    return (
      <View style={{ padding: 10 }}>
        <View>
          <TextInput
            style={styles.app_input}
            placeholder="Napisz coś!"
            onChangeText={(text) => this.setState({ text })}
          />
        </View>
        <View>
          <Button
            onPress={() => {
              Alert.alert(this.getAlertText());
            }}
            title="Naciśnij mnie!"
          />
        </View>
      </View>
    );
  }
}

export default class App extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <View>
          <Image source={require('./salt.png')} />
        </View>
        <View >
          <Text style={styles.red}>Hello, world!</Text>
        </View>
        <AlertConfimation />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  red: {
    color: 'red',
    fontWeight: 'bold',
    fontSize: 30,
  },
  app_input: {
    fontWeight: 'bold',
    fontSize: 30,
    height: 40,
  }
});
