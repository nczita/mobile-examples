import React, { Component } from 'react';
import { Layout, Header, Drawer, Content } from 'react-mdl';
import VitaList from './components/VitaList';
import './App.css';

class App extends Component {
  render() {
    return (
      <Layout className="App">
          <Header title="VitaApp">
          </Header>
          <Drawer title="VitaApp">
          </Drawer>
          <Content>
            <VitaList />
          </Content>
      </Layout>
    );
  }
}

export default App;
