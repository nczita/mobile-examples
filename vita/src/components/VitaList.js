import React, { Component } from 'react';
import { List, ListItem, ListItemContent } from 'react-mdl';
import axios from 'axios';

class VitaList extends Component {
    constructor() {
        super();
        this.state = {
            saints: []
        };
    }

    componentWillMount() {
        axios.get('/saints.json')
            .then(res => {
                const saints = res.data;
                this.setState({ saints });
            });
    }

    render() {
        return (
            <List>
                {this.state.saints.map(saint =>
                    <ListItem threeLine>
                        <ListItemContent subtitle={saint.desc}>{saint.name}</ListItemContent>
                    </ListItem>
                )}
            </List>
        );
    }
}

export default VitaList;