package pl.saltsoft.ketchupmenu;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class MenuActivity extends AppCompatActivity {
    private static final String TAG = "MenuActivity";
    private String menuUrl = "http://192.168.1.12:5544";
    private ListView listView;
    private TextView infoText;
    ArrayList<MenuModel> menuModelArrayList;
    private MenuAdapter menuAdapter;
    private static ProgressDialog mProgressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        listView = findViewById(R.id.menu);
        infoText = findViewById(R.id.info);
        getMenuJson(this);
    }

    protected void getMenuJson(final Context context) {
        showSimpleProgressDialog(context, "Loading...", "Fetching Json", false);
        Log.v(TAG, "getMenuJson");
        RequestQueue queue = Volley.newRequestQueue(this);
        JsonArrayRequest jsonObjectRequest = new JsonArrayRequest(
                Request.Method.GET, menuUrl, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.v(TAG, "onResponse");
                        removeSimpleProgressDialog();
                        menuModelArrayList = getInfo(response);
                        menuAdapter = new MenuAdapter(context, menuModelArrayList);
                        listView.setAdapter(menuAdapter);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.v(TAG, "onErrorResponse");
                        Log.v(TAG, error.getMessage());
                        Toast.makeText(MenuActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
        );
        Log.v(TAG, "queue.add");
        queue.add(jsonObjectRequest);
    }

    public ArrayList<MenuModel> getInfo(JSONArray response) {
        ArrayList<MenuModel> menuModelArrayList = new ArrayList<>();
        try {
            for (int i = 0; i < response.length(); i++) {
                MenuModel playersModel = new MenuModel();
                JSONObject dataobj = response.getJSONObject(i);
                playersModel.setName(dataobj.getString("name"));
                playersModel.setDescription(dataobj.getString("description"));
                playersModel.setPrice(dataobj.getDouble("price"));
                menuModelArrayList.add(playersModel);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return menuModelArrayList;
    }

    public static void showSimpleProgressDialog(Context context, String title,
                                                String msg, boolean isCancelable) {
        try {
            if (mProgressDialog == null) {
                mProgressDialog = ProgressDialog.show(context, title, msg);
                mProgressDialog.setCancelable(isCancelable);
            }

            if (!mProgressDialog.isShowing()) {
                mProgressDialog.show();
            }

        } catch (IllegalArgumentException ie) {
            ie.printStackTrace();
        } catch (RuntimeException re) {
            re.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void removeSimpleProgressDialog() {
        try {
            if (mProgressDialog != null) {
                if (mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                    mProgressDialog = null;
                }
            }
        } catch (IllegalArgumentException ie) {
            ie.printStackTrace();

        } catch (RuntimeException re) {
            re.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
