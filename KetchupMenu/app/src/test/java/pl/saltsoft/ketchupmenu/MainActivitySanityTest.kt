package pl.saltsoft.ketchupmenu

import android.widget.TextView
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

import org.junit.runner.RunWith
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import android.content.Intent
import android.widget.Button
import org.robolectric.Shadows.shadowOf


@RunWith(RobolectricTestRunner::class)
@Config(sdk = [25], manifest = "AndroidManifest.xml")
class MainActivitySanityTest {
    @Test
    fun itShouldMoveToMenuActivity() {
        val activity = Robolectric.setupActivity(MainActivity::class.java)
        activity.findViewById<Button>(R.id.button3).performClick();
        val expectedIntent = Intent(activity, MenuActivity::class.java)
        val actualIntent = shadowOf(activity).getNextStartedActivity()
        assertThat(expectedIntent.filterEquals(actualIntent)).isTrue();
    }
}
